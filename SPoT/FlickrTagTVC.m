//
//  FlickrTagTVC.m
//  SPoT
//
//  Created by Stephan Kristiansen on 8/8/13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "FlickrTagTVC.h"
#import "FlickrFetcher.h"

@interface FlickrTagTVC () <UISplitViewControllerDelegate>

@end

@implementation FlickrTagTVC

// sets the Model
// reloads the UITableView (since Model is changing)

- (void)setPhotos:(NSArray *)photos
{
    _photos = photos;
    
    NSMutableDictionary *photoTags = [[NSMutableDictionary alloc] init];
    
    //find tagcount
    for (NSDictionary *photo in _photos) {
        if([photo[FLICKR_TAGS] isKindOfClass:[NSString class]])
        {
            NSString *tagsForPhoto = (NSString *)photo[FLICKR_TAGS];            
            NSArray *tagsForPhotoArray = [tagsForPhoto componentsSeparatedByString:@" "];
            
            NSMutableArray *mutableVersion = [tagsForPhotoArray mutableCopy];
            [mutableVersion removeObjectIdenticalTo:@"cs193spot"];
            [mutableVersion removeObjectIdenticalTo:@"portrait"];
            [mutableVersion removeObjectIdenticalTo:@"landscape"];
            
            for (NSString *tag in tagsForPhotoArray)
            {
                int countForThisTag = 1;
                if([photoTags objectForKey:tag])
                {
                    countForThisTag = [(NSNumber *)[photoTags objectForKey:tag] intValue] + 1;
                }
                
                [photoTags setObject:[[NSNumber alloc] initWithInt:countForThisTag] forKey:tag];
            }
        }
    }
    
    //make array of dictionaries with keys "title" and "tagcount"
    NSMutableArray *tagAndCountArray = [[NSMutableArray alloc] init];
    for (NSString *tag in photoTags.allKeys)
    {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjects:[[NSArray alloc] initWithObjects:tag, [photoTags objectForKey:tag], nil] forKeys:[[NSArray alloc] initWithObjects:@"title", @"tagcount", nil]];
        [tagAndCountArray addObject:dict];
    }
    
    if (tagAndCountArray) {       
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
        
        self.tagsAndCount = [tagAndCountArray sortedArrayUsingDescriptors:sortDescriptors];
    }
    
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"Show imagelist"]) {
                if ([segue.destinationViewController respondsToSelector:@selector(setPhotos:)])
                {
                    NSMutableArray *photosWithSelectedTag = [[NSMutableArray alloc] init];
                    NSString *tagChosen = [self titleForRow:indexPath.row];
                    
                    for (NSDictionary *photoDict in self.photos) {
                        if([photoDict[FLICKR_TAGS] isKindOfClass:[NSString class]])
                        {
                            NSString *tagsForPhoto = (NSString *)photoDict[FLICKR_TAGS];
                            NSArray *tagsForPhotoArray = [tagsForPhoto componentsSeparatedByString:@" "];
                            
                            for (NSString *tag in tagsForPhotoArray)
                            {
                                if([tag isEqualToString:[tagChosen lowercaseString]])
                                {
                                    [photosWithSelectedTag addObject:photoDict];
                                    break;
                                }
                            }
                        }
                    }
                    
                    [segue.destinationViewController performSelector:@selector(setPhotos:) withObject:photosWithSelectedTag];
                    
                    [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
                    
                }
            }
        }
    }
}

#pragma mark - UISplitViewControllerDelegate

- (void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

- (BOOL)splitViewController:(UISplitViewController *)svc
   shouldHideViewController:(UIViewController *)vc
              inOrientation:(UIInterfaceOrientation)orientation
{
    return NO;
}

#pragma mark - UITableViewDataSource

// lets the UITableView know how many rows it should display
// in this case, just the count of dictionaries in the Model's array

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tagsAndCount count];
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the title out of it

- (NSString *)titleForRow:(NSUInteger)row
{
    //return [self.tags[row][FLICKR_TAGS] description]; // description because could be NSNull
    return [[self.tagsAndCount[row][@"title"] description] capitalizedString];
}

// a helper method that looks in the Model for the photo dictionary at the given row
//  and gets the owner of the photo out of it

- (NSString *)subtitleForRow:(NSUInteger)row
{
    //return [self.photos[row][FLICKR_PHOTO_OWNER] description]; // description because could be NSNull
    return [self.tagsAndCount[row][@"tagcount"] description];
}

// loads up a table view cell with the title and owner of the photo at the given row in the Model

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FlickrTag";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [self titleForRow:indexPath.row];
    cell.detailTextLabel.text = [self subtitleForRow:indexPath.row];
    
    return cell;
}

@end
