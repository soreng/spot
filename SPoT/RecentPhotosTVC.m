//
//  RecentPhotosTVC.m
//  SPoT
//
//  Created by Stephan Kristiansen on 8/8/13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "RecentPhotosTVC.h"
#import "FlickrFetcher.h"

@interface RecentPhotosTVC ()

@end

@implementation RecentPhotosTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.photos = [self getRecentPhotosFromNSDefault];
    [self setTitle:@"Most recent photos"];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.photos = [self getRecentPhotosFromNSDefault];
}

- (NSArray *)getRecentPhotosFromNSDefault
{
    NSDictionary *recentPhotosFromUserDefaults = [[NSUserDefaults standardUserDefaults] dictionaryForKey:RECENT_PHOTOS_KEY];
    
    NSArray *allRecentPhotos = recentPhotosFromUserDefaults.allValues;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:TIMEVIEWED ascending:NO];    
    NSArray *recentPhotosSorted = [allRecentPhotos sortedArrayUsingDescriptors:@[sortDescriptor]];

    NSMutableArray *photos = [[NSMutableArray alloc] init];
    for (NSDictionary *recent in recentPhotosSorted) {
        [photos addObject:[recent objectForKey:FLICKR_PHOTO]];
    }
    
    return photos;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"Show image"]) {
                if ([segue.destinationViewController respondsToSelector:@selector(setImageURL:)]) {
                    NSURL *url = [FlickrFetcher urlForPhoto:self.photos[indexPath.row] format:FlickrPhotoFormatLarge];
                    [segue.destinationViewController performSelector:@selector(setImageURL:) withObject:url];
                    [segue.destinationViewController setTitle:[self titleForRow:indexPath.row]];
                }
            }
        }
    }
}

@end
