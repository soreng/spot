//
//  main.m
//  SPoT
//
//  Created by Stephan Kristiansen on 8/8/13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
