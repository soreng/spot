//
//  FlickrTagTVC.h
//  SPoT
//
//  Created by Stephan Kristiansen on 8/8/13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrTagTVC : UITableViewController

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *tagsAndCount;

@end
